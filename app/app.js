import Vue from "nativescript-vue";
import firebase from "nativescript-plugin-firebase";

import Home from "./components/Home";
// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === "production");

firebase.init()
    .then(() => console.log("Firebase initialized"))
    .catch(error => console.log("Error initializing Firebase: " + error));

new Vue({

    template: `
        <Frame>
            <Home />
        </Frame>`,

    components: {
        Home
    }
}).$start();
